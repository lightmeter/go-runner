# Go Runner

A helper package for managing parallell tasks.

This code has been extracted from Lightmeter ControlCenter, to make it easier to be reused:

https://gitlab.com/lightmeter/controlcenter/-/tree/master/pkg/runner

## TODO

- Document it
- Cleanup API
- Setup CI/CD Stuff
