// SPDX-FileCopyrightText: 2021 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: AGPL-3.0-only

package runner

func mustSucceed(err error) {
	if err != nil {
		panic(err)
	}
}

// TODO: handle errors!!!
func NewDependantPairCancellableRunner(dependency CancellableRunner, dependant CancellableRunner) CancellableRunner {
	return NewCancellableRunner(func(done DoneChan, cancel CancelChan) {
		dependencyDone, dependencyCancel := Run(dependency)
		dependantDone, dependantCancel := Run(dependant)

		go func() {
			<-cancel
			dependencyCancel()
			mustSucceed(dependencyDone())
			dependantCancel()
		}()

		go func() {
			mustSucceed(dependantDone())
			done <- nil
		}()
	})
}
